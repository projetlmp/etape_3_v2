from src.custom_class.entities import EntitiesSet
from src.custom_class.corpus import Corpus
from src.entities_set_processing.entities_set_processor import EntitiesProcessor
from src.load_corpus import load_corpus
from utils.utils_process_entities import process_entities_types
from utils.utils_for_string import to_standard
from utils.utils_for_names import choose_name
from src.gestion_contextes import choose_most_relevant_context, format_contexte, sort_chronologically
from input.topics import topics

corpus = Corpus(load_corpus('/home/marjolaine/Documents/Influenceurs/Elan_Etape_2'))
assert (len(corpus.dict_articles) != 0)
entities = EntitiesSet(corpus=corpus)
entities = EntitiesSet(dict_entities = {k: entities.dict_entities[k] for k in entities.dict_entities if k in list(entities.dict_entities.keys())[0:10]})
assert (len(entities.dict_entities) != 0)
output_dir = "/home/marjolaine/Documents/Influenceurs/Elan_Etape_3"
entities_processor = EntitiesProcessor(entities)
entities_processor.add_contextes(corpus, nb_sentences=1)
entities_processor.remove_redundant_contexts(corpus)
with open('/home/marjolaine/Documents/Influenceurs/etape_3_v2/input/codes_insee') as f:
    content = f.readlines()
content = [x.strip() for x in content]

entities_processor.entities_set = process_entities_types(entities_processor.entities_set, overwrite=False,
                                                         codes_departement=['87'],
                                                         codes_insee_commune=content,
                                                         codes_region=['75'])

with open("/home/marjolaine/Documents/Influenceurs/etape_3_v2/input/list_locations_of_interest.txt") as f:
    loc = f.readlines()

loc = [c.replace('\n', '') for c in loc]
dict_loc = {to_standard(k): k for k in loc}

themes = topics.keys()

for ent in entities_processor.entities_set.iter_entities():
    ent.themes = {theme: 0 for theme in themes}
    ent.pertinence = 0
    for article in ent.articles:
        themes = corpus.dict_articles[article]['themes']
        for theme in themes:
            if theme in ent.themes:
                ent.themes[theme] += 1
            else:
                ent.themes[theme] = 1
        ent.pertinence = ent.pertinence + 1 if len(themes) > 0 else ent.pertinence

directory_name = output_dir
final = []
for id_entity in entities_processor.entities_set.dict_entities:
    if entities_processor.entities_set.dict_entities[id_entity].elu or entities_processor.entities_set.dict_entities[
        id_entity].asso or entities_processor.entities_set.dict_entities[id_entity].autre:
        json_file = {}
        json_file['id'] = id_entity
        json_file['name'] = choose_name(entities_processor.entities_set.dict_entities[id_entity].noms)
        json_file['is_elu'] = 1 if entities_processor.entities_set.dict_entities[id_entity].elu else 0
        json_file['mandates'] = []
        for i, mandat in enumerate(entities_processor.entities_set.dict_entities[id_entity].mandats):
            json_file['mandates'].append(mandat.__to_dict__())
        json_file['is_association'] = 1 if entities_processor.entities_set.dict_entities[id_entity].asso else 0
        json_file['association_description'] = entities_processor.entities_set.dict_entities[id_entity].desc_asso
        json_file['name_loc_asso'] = entities_processor.entities_set.dict_entities[id_entity].name_loc_asso
        json_file['date_creat_asso'] = entities_processor.entities_set.dict_entities[id_entity].date_creat_asso
        json_file['is_other'] = 1 if (entities_processor.entities_set.dict_entities[id_entity].autre and not
        entities_processor.entities_set.dict_entities[id_entity].elu) else 0
        json_file['contextes'] = sort_chronologically(
            format_contexte(
                choose_most_relevant_context(entities_processor.entities_set.dict_entities[id_entity].contextes)))
        json_file['total_mentions'] = len(json_file['contextes'])
        json_file['pertinence'] = len([i for i in entities_processor.entities_set.dict_entities[id_entity].articles if
                                       len(corpus.dict_articles[i]['themes']) > 0])
        json_file['themes'] = [
            {'name': theme, 'pertinence': entities_processor.entities_set.dict_entities[id_entity].themes[theme]} for
            theme
            in entities_processor.entities_set.dict_entities[id_entity].themes if
            entities_processor.entities_set.dict_entities[id_entity].themes[theme] > 0]
        if json_file['is_association']:
            json_file['main_location'] = entities_processor.entities_set.dict_entities[id_entity].name_loc_asso
        if json_file['is_elu']:
            json_file['main_location'] = json_file['mandates'][0]['nom_loc']
        else:
            json_file['main_location'] = ''
        json_file['location'] = [dict_loc[i] for i in entities_processor.entities_set.dict_entities[id_entity].lieux if
                                 i in dict_loc]
        final.append(json_file)

persons_to_drop = []

assos_to_keep = []

final = sorted(final, key=lambda k: k['total_mentions'], reverse=True)
new_final = [final[i] for i in range(len(final)) if final[i]["id"] not in persons_to_drop]
new_new_final = new_final[0:100] + [i for i in new_final[100:len(new_final) - 1] if (
        ['id'] in assos_to_keep or i['is_elu'] == 1)]

for f in new_new_final:
    if f['total_mentions'] != len(f['contextes']):
        print('pb')

for f in new_new_final:
    if f['total_mentions'] < f['pertinence']:
        print('pb')
