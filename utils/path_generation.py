import os
from config.path import PROJECT_PATH, path_source_keys, path_output_data, path_input_data, \
    data_elus_path, data_assos_path, path_to_data


def get_preprocess_path(filename):
    return os.path.join(PROJECT_PATH, 'preprocess', filename)


def get_input_path(filename):
    return os.path.join(PROJECT_PATH, 'input', filename)


def get_source_dic_path(filename='DJ_Sources_Titles_Keys.csv'):
    return os.path.join(PROJECT_PATH, path_source_keys, filename)


def get_output_data_path(filename):
    return os.path.join(path_output_data, filename)


def get_input_data_path():
    return path_input_data


def get_data_elus_path():
    return os.path.join(path_to_data, data_elus_path)


def get_data_asso_path():
    return os.path.join(path_to_data, data_assos_path)


def get_data_codes_insee():
    return path_to_data

def get_list_loc_of_interest():
    return get_input_path("list_locations_of_interest.txt")