from src.custom_class.contextes import Contextes

def load_saved_contexts(list_contexts):
    contexts = []
    for c in list_contexts:
        contexts.append(Contextes(titre=c['title'], text=c['text'], id_article=c['id'], date=c['date'], source=c['source'], themes=c['themes']))
    return contexts