from utils.path_generation import get_data_elus_path
from src.custom_class.mandat import Mandat
from src.custom_class.elu import Elu, ElusSet
import os
import json


def elus_set_from_json(data):
    dict_elus = {}
    for elu_id in data.keys():
        mandats = [
            Mandat(loc=data[elu_id]['mandats'][mandat_i]['loc'], nom_loc=data[elu_id]['mandats'][mandat_i]['nom_loc'], fonction=data[elu_id]['mandats'][mandat_i]['fonction'],
                   couleur_politique=data[elu_id]['mandats'][mandat_i]['couleur_politique']) for mandat_i in
            data[elu_id]['mandats']]
        dict_elus[elu_id] = Elu(data[elu_id]['nom'], data[elu_id]['prenom'], mandats, elu_id)
    return ElusSet(dict_elus=dict_elus)


def load_saved_data_elus(input_file='ElusSet.json'):
    with open(os.path.join(get_data_elus_path(), input_file)) as f:
        data = json.load(f)
    return elus_set_from_json(data)
