import json
import os
from config.path import path_output_data
from src.custom_class.entities import EntitiesSet, Entity
from utils.load_saved_data_contexts import load_saved_contexts


def entities_set_from_json(data):
    dict_entities = {}
    for ent in data:
        dict_entities[ent['id']] = Entity(noms=ent['name'], type=ent['type_entity'],
                                          articles=ent['articles'],
                                          contextes=load_saved_contexts(ent['contextes']),
                                          asso=ent['is_association'], elu=ent['is_elu'],
                                          autre=ent['is_other'],
                                          locations=set(ent['location']),
                                          )
    return EntitiesSet(dict_entities=dict_entities)


def load_saved_data_entities(path):
    with open(path) as f:
        data = json.load(f)
    return entities_set_from_json(data)
