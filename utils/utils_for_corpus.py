from src.custom_class.corpus import Corpus


def get_corpus(entity, corpus):
    """
    :param entity:
    :param corpus:
    :return: corpus of articles containing the entity, identified by entity.article
    """
    ids = entity.articles
    return Corpus(dict_articles={id: corpus.dict_articles[id] for id in ids})
