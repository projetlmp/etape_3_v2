from utils.path_generation import get_list_loc_of_interest
import os
from utils.utils_for_string import to_standard

def get_processed_list_locations_of_interest():
    loaded_loc = open(get_list_loc_of_interest(), 'r')
    list_loc_of_interest = [line.rstrip(os.linesep) for line in loaded_loc]
    processed_list_loc_of_interest = set([to_standard(loc) for loc in list_loc_of_interest])
    return processed_list_loc_of_interest