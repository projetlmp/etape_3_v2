import unidecode
import re


def remove_small_chunks(text):
    chunks = text.split(' ')
    output = []
    for c in chunks:
        if len(c) >= 2:
            output.append(c)
    return ' '.join(output)


def remove_prefix(text):
    return text.replace('Mmes ', '').replace('Mme ', '').replace('Dr ', '')

def to_standard(text, no_prefix=False):
    if isinstance(text, str):
        if no_prefix:
            text = remove_prefix(text)
        text = unidecode.unidecode(text)
        text = ''.join(e for e in text if e.isalnum())
        text = text.lower()
        return text
    else:
        return False


caps = "([A-Z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|Je|Tu|Il|Elle|Ils|Elles|On|But\s|However\s|That\s|This\s|Wherever)F"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"


def quotation_marks_specific(text):
    i = 0
    while i < len(text):
        if text[i] == '«':
            while i < len(text) and text[i] != '»':
                if text[i] == "?":
                    text = text[0:i] + "<ppp>" + text[i + 1:]
                if text[i] == "!":
                    text = text[0:i] + "<ppd>" + text[i + 1:]
                if text[i] == ".":
                    text = text[0:i] + "<prd>" + text[i + 1:]
                i += 1
        i += 1
    return text


def wrappers_specif(text):
    wrappers = ['}', ')', ']']
    for w in wrappers:
        text = text.replace('...' + w, "<prd><prd><prd>" + w)
        text = text.replace("." + w, "<prd>" + w + "<stop>")
        text = text.replace("!" + w, "<prd>" + w + "<stop>")
        text = text.replace("?" + w, "<prd>" + w + "<stop>")
        text = text.replace("<stop>" + ' ' + w, w + "<stop>")
        text = text.replace("!" + w, "<ppd>" + w + "<stop>")
        text = text.replace("?" + w, "<ppp>" + w + "<stop>")
    return text


def split_into_sentences(text):
    text = " " + text + "  "
    text = re.sub(prefixes, "\\1<prd>", text)
    text = re.sub(websites, "<prd>\\1", text)
    if "Ph.D" in text: text = text.replace("Ph.D.", "Ph<prd>D<prd>")
    text = quotation_marks_specific(text)
    text = wrappers_specif(text)
    text = text.replace(". »", "<prd> »<stop>")
    text = text.replace("...", "<prd><prd><prd><stop>")
    text = re.sub("\s" + caps + "[.] ", " \\1<prd> ", text)
    text = re.sub(acronyms + " " + starters, "\\1<stop> \\2", text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>\\3<prd>", text)
    text = re.sub(caps + "[.]" + caps + "[.]", "\\1<prd>\\2<prd>", text)
    text = re.sub(" " + suffixes + "[.] " + starters, " \\1<stop> \\2", text)
    text = re.sub(" " + suffixes + "[.]", " \\1<prd>", text)
    text = re.sub(" " + caps + "[.]", " \\1<prd>", text)
    text = text.replace(".", ".<stop>")
    text = text.replace("?", "?<stop>")
    text = text.replace("!", "!<stop>")
    text = text.replace("<prd>", ".")
    text = text.replace("ppd", "!")
    text = text.replace("ppp", "?")
    if text[(len(text) - 6):(len(text))] != "<stop>": text = text + '<stop>'
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences


def clean_name(nom):
    nom_nett = nom.replace("_ ", '')
    nom_nett = nom_nett.replace(" _", '')
    nom_nett = nom_nett.replace(" \"", '')
    nom_nett = nom_nett.replace("\" ", '')
    nom_nett = nom_nett.replace(" \'", '')
    nom_nett = nom_nett.replace("\' ", '')
    nom_nett = nom_nett.replace("«", '')
    nom_nett = nom_nett.replace("«", '')
    nom_nett = nom_nett.replace("»", '')
    nom_nett = nom_nett.replace("»", '')
    nom_nett = remove_small_chunks(nom_nett)
    nom_nett = ' '.join([i[0] + i[1:].lower() if (len(i) > 0 and '-' not in i) else i for i in nom_nett.split(' ')])
    return nom_nett