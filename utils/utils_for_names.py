import wikipedia
import re
from utils.utils_string_manipulation import to_standard
from utils.utils_string_manipulation import clean_name
from utils.utils import get_list_from_file
from utils.path_generation import get_input_path


def get_original_location_name(standard_location_name):
    """
    :param standard_location_name:
    :return: original location name before normalisation
    """
    list_loc_of_interest = get_list_from_file(get_input_path('list_locations_of_interest.txt'))
    for original_location_name in list_loc_of_interest:
        if to_standard(original_location_name) == standard_location_name:
            return original_location_name
    return 'not in list_loc_of_interest'


def is_dead(name):
    """
    True si la personne est morte, False sinon
    """
    wikipedia.set_lang('fr')
    try:
        page = wikipedia.page(name)
        sumup = page.summary
        if to_standard(sumup).find(to_standard(name)) != -1:
            pattern = re.compile(r"(et ){,1}(morte?|assassinée?|tuée?)( le){,1}")
            is_dead = re.search(pattern, sumup)
            if is_dead:
                return True
            else:
                return False
        else:
            return False
    except wikipedia.exceptions.PageError:
        pass
    except wikipedia.exceptions.DisambiguationError:
        pass


def choose_name(names):
    good_name = ''
    for name in names:
        if len(clean_name(name)) > len(
                good_name):
            good_name = clean_name(name)
            return good_name
    return good_name
