from utils.path_generation import get_data_asso_path
from src.custom_class.asso import AssoSet, Asso
import os
import json


def assos_set_from_json(data):
    dict_assos = {}
    for asso_id in data.keys():
        dict_assos[asso_id] = Asso(nom=data[asso_id]['nom'], loc=data[asso_id]['loc'], id=data[asso_id]['id'],
                                   objet=data[asso_id]['objet'], name_loc=data[asso_id]['name_loc'], date_creat=data[asso_id]['date_creat'])
    return AssoSet(dict_assos=dict_assos)


def load_saved_data_assos(input_file='AssosSet.json'):
    with open(os.path.join(get_data_asso_path(), input_file)) as f:
        data = json.load(f)
    return assos_set_from_json(data)
