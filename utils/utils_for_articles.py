from utils.utils_for_string import to_standard
from utils.utils import get_list_from_file
from utils.path_generation import get_input_path


def clean_loc_article(article):
    """
    remove fields in article['loc'] that are not in list_loc_of_interest
    :param article:
    :return:
    """
    list_loc_of_interest = get_list_from_file(get_input_path('list_locations_of_interest.txt'))
    normalised_list_loc_of_interest = set([to_standard(loc) for loc in list_loc_of_interest])
    article['loc'] = {loc_name: article['loc'][loc_name] for loc_name in article['loc'] if
                      loc_name in set(article['loc'].keys()).intersection(normalised_list_loc_of_interest)}
    return article
