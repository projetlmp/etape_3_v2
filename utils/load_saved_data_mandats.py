from src.custom_class.mandat import Mandat


def load_saved_data_mandats(list_mandats):
    mandats = []
    for mandat in list_mandats:
        mandats.append(Mandat(loc=mandat['loc'], nom_loc= mandat['nom_loc'],  fonction=mandat['fonction'],
                              couleur_politique=mandat['couleur_politique']))
    return mandats
