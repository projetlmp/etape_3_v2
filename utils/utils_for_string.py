import unidecode
import re

def remove_prefix(text):
    return text.replace('Mmes ', '').replace('Mme ', '').replace('Dr ', '')


def find_regex(regex):
    return re.compile(regex, flags=re.IGNORECASE).search


def find_whole_word(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search


def to_standard(text, no_prefix=False):
    if isinstance(text, str):
        if no_prefix:
            text = remove_prefix(text)
        text = unidecode.unidecode(text)
        text = ''.join(e for e in text if e.isalnum())
        text = text.lower()
        return text
    else:
        return False