from src.load_data_elus import load_data_elus, filter_data_elus_by_loc
from src.load_data_assos import load_data_assos, filter_data_assos_by_loc


def process_entities_for_loc(entities_processor, corp, nb_sentences, output_dir='output_test_final_entities'):
    """

    :param locations:
    :param entities_processor:
    :param corp:
    :param nb_sentences:
    :param output_dir:
    :return:
    """
    entities_processor.keep_only_persons_size_two()
    entities_processor.keep_only_persons()
    entities_processor.keep_only_alive_persons()
    entities_processor.filter_public_places()
    entities_processor.add_cooccurrence()
    entities_processor.save(output_dir)


def process_entities_types(entities_set, overwrite=False, codes_departement=[], codes_insee_commune=[],
                           codes_region=[]):
    """

    :param entities_set:
    :param overwrite:
    :param codes_departement: list of strings of each departement code. eg: ['73', '74', '75']
    :param codes_insee_commune:
    :param codes_region:
    :return:
    """
    elus_set = load_data_elus(overwrite)
    elus_set = filter_data_elus_by_loc(elus_set, codes_departement, codes_insee_commune, codes_region)
    assos_set = load_data_assos(overwrite)
    assos_set = filter_data_assos_by_loc(assos_set, codes_departement, codes_insee_commune, codes_region)
    for i, entity in enumerate(entities_set.iter_entities()):
        print(' {} / {}'.format(i, len(entities_set.dict_entities)))
        entity.is_autre()
        if entity.autre:
            entity.is_elu(elus_set)
        entity.is_asso(assos_set)

    return entities_set
