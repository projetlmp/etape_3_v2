import os
import json
from utils.utils import SetEncoder


def get_article_path(output_path, article_id):
    date_str = article_id.split('_')[1]
    year = date_str[4:9]
    month = date_str[2:4]
    return os.path.join(output_path, year, month, article_id)


def save_updated_article(corpus_dict, output_path, article_id):
    path_article = get_article_path(output_path, article_id)
    if not os.path.isdir(os.path.dirname(os.path.dirname(os.path.dirname(path_article)))):
        os.makedirs(os.path.dirname(os.path.dirname(os.path.dirname(path_article))))
    if not os.path.isdir(os.path.dirname(os.path.dirname(path_article))):
        os.makedirs(os.path.dirname(os.path.dirname(path_article)))
    if not os.path.isdir(os.path.dirname(path_article)):
        os.makedirs(os.path.dirname(path_article))
    js = json.dumps(corpus_dict, cls=SetEncoder)
    fp = open(os.path.join(path_article), 'w')
    fp.write(js)

