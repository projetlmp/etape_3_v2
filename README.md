# Influenceurs - Etape 3

Entrée : corpus d'articles de presse après [l'étape 2](https://bitbucket.org/Marjolaine_Grunenberger/etape_2/src/master/) 

Sortie : les deux fichiers suivants dans le repertoire dont le nom et indiqué dans le main : 

*  cooccurrence_matrix.json : coocurrence_matrix['nom_standardisé_1 (TYPE)']['nom_standardisé_2 (TYPE)'] = card(articles communs).

La fonction de standardisation des noms to_standarr est celle définie [ici](https://bitbucket.org/Marjolaine_Grunenberger/etape_1/src/master/utils/utils_string_manipulation.py).

* entities.json : un dataframe pandas au format json contenant les colonnes ["Name", "Type", "Cleaned Contextes", "Mentions totales", "Mentions locales", "Locations"]. 

    * Name : les différentes orthographes associées à un même nom standardisé ;
    * Type : le type de l'entité (se référer au dictionnaire source - lmp dans le dossier configuration) ;
    * Cleaned Contextes : contextes de l'entité contenus dans les articles co-occurents avec le ou les lieux d'intérêts listés dans list_loc_of_interest ;
    * Mentions totales : mentions de l'entité dans l'ensembles des articles obtenus à la fin de l'étape 2 ; 
    * Mentions locales : mentions de l'entité dans des articles co-occurents avec le ou les lieux d'intérêts listés dans list_loc_of_interest ;
    * Locations : les lieux co-occurents vec les différentes orthographes de l'entité. 

## Pour commencer

### Prérequis

Télécharger les vecteurs pré-entrainés fasttext français [ici](https://github.com/facebookresearch/fastText/blob/master/pretrained-vectors.md).

Créer un environement virtuel et l'activer. 

```
sudo pip install virtualenv
virtualenv venv
source venv/bin/activate
```

Installer les requierements. 

```
pip install -r requierements.txt
```

### Remplacer les chemins par défaut

Dans config/path.py remplacer les chemins par les chemins correspondant sur votre ordinateur.

path_output : là où le repertoire contenant les entités et la matrice de co-occurrence seront stockés. 

path_test_articles : les articles utilisés dans les tests unitaires. 

path_to_vec  : là où les vecteurs fasttext sont stockés. 

path_article_base : là où se situent les corpus pré-processés en sortie de l'étape 2. 

### Paramétrer les thèmes

Dans input/topics mettre le même fichier que dans l'étape 2.

## Extraire les entités 
* Par défaut : 

exemple : 
```
python main.py

```

lance l'étape trois en prenant pour entrée les articles situés dans article_base_path (*attention : doit être identique à path_output de l'étape 2) from config/path.py et les sauves
dans path_output/'output_final_entities', path_output from config/path.py. 


* Changer le chemin de sortie : 


exemple : 
```
python main.py  -o 'path/to/saved/output' 
```

* Diviser les repertoires de sortie par emplacement :

exemple : 

```
python main.py -d 

```

* Ajouter des phrases de contextes à gauche et à droite de la phrase d'occurence de l'entité pour chaque entité :

exemple : 

```
python main.py -c 2

```

ajoutera deux phrases avant et après la phrase d'occurrence de l'entité. 


## Lancer les trois étapes

*Attention : le chemin output_path de l'étape 1 doit être idetique à path_article_base de l'étape 2, et path_output de l'étape 2 doit être identique à path_input de l'étape 3.*

Dans le répertoire contenant les tros étapes, tapez en ligne de commande : 

```
python etape_1/main.py -dir 'path/to/corpus' | python etape_2/main.py | python etape_3/main.py

```