from src.custom_class.entities import EntitiesSet
from src.custom_class.corpus import Corpus
from utils.path_generation import get_input_data_path, get_input_path
from src.entities_set_processing.entities_set_processor import EntitiesProcessor
from src.load_corpus import load_corpus
from utils.utils import get_list_from_file
from utils.utils_process_entities import process_entities_types

def process_entities(list_loc_of_interest):
    entities_processor = EntitiesProcessor(entities)
    entities_processor.filtered_entities_loc(list_loc_of_interest)
    entities_processor.add_contextes(corpus)
    entities_processor.remove_redundant_contexts(corpus)
    entities_processor.entities_set = process_entities_types(entities_processor.entities_set, overwrite=False,
                                                             codes_departement=[], codes_insee_commune=[],
                                                             codes_region=[])
    entities_processor.entities_set.save(corpus, output_directory_name, production=False)

if __name__ == '__main__':
    divide_outputs_by_loc = False
    list_loc_of_interest = get_list_from_file(get_input_path('list_locations_of_interest.txt'))
    output_directory_name = 'Output_Etape_3_v0'
    article_base_path = get_input_data_path()
    corpus = Corpus(dict_articles=load_corpus(article_base_path))
    corpus.clean_loc_corpus()
    entities = EntitiesSet(corpus=corpus)
    if divide_outputs_by_loc:
        for loc in list_loc_of_interest:
            process_entities([loc])
    else:
            process_entities(list_loc_of_interest)