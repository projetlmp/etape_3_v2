from utils.utils_for_string import to_standard
from utils.path_generation import get_input_path
import os

loc = open(get_input_path('list_locations_of_interest.txt'), 'r')
list_loc_of_interest = [line.rstrip(os.linesep) for line in loc]
dict_loc = {to_standard(loc): loc for loc in list_loc_of_interest}
