import unittest
from src.load_data_assos import load_data_assos

class TestAssos(unittest.TestCase):
    def setUp(self):
        self.assos = load_data_assos(overwrite=True, output_file='TestAssosSet.json')
        self.loaded_assos = load_data_assos(overwrite=False, input_file='TestAssosSet.json')


    def test_loaded_assos(self):
        self.assertEqual(len(self.assos.dict_assos.keys()), len(self.loaded_assos.dict_assos.keys()))
        self.assertEqual(self.assos.dict_assos["W632007046"].nom, 'BLACK OWL')
        self.assertEqual(self.loaded_assos.dict_assos['W033002617'].loc, '03023')
        self.assertEqual(self.assos.dict_assos['W332006701'].id, 'W332006701')
        self.assertEqual(self.assos.dict_assos['W772005478'].objet[0:4], 'ayan')

