import unittest
from src.load_updated_corpus import load_corpus


class TestLoadCorpus(unittest.TestCase):
    def setUp(self):
        self.corpus = load_corpus('test_ouput_2_theme', sources = None, date_min = '1000/01', date_max='3000/01')


    def test_corpus_load(self):
        self.assertEqual(list(self.corpus['ACPUB_14012011_49242021_test_theme.json'].keys()), ['id', 'modification_datetime', 'ingestion_datetime', 'publication_date', 'date', 'snippet', 'corps', 'art', 'action', 'credit', 'byline', 'document_type', 'language_code', 'titre', 'copyright', 'dateline', 'source_code', 'modification_date', 'section', 'company_codes', 'publisher_name', 'region_of_origin', 'word_count', 'sujet', 'region_codes', 'industry_codes', 'person_codes', 'currency_codes', 'market_index_codes', 'company_codes_about', 'company_codes_association', 'company_codes_lineage', 'company_codes_occur', 'company_codes_relevance', 'source', 'entities', 'loc', 'themes'], 'wrong keys in updated_corpus[id]')
