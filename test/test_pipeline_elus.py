import unittest
from utils.utils_process_entities import filter_entities_by_loc
from src.load_updated_corpus import load_corpus

from src.custom_class.corpus import Corpus
from src.custom_class.entities import EntitiesSet
from src.entities_set_processing.entities_set_processor import EntitiesProcessor
from src.load_data_elus import load_data_elus


class TestPipelineElus(unittest.TestCase):
    def setUp(self):
        self.corpus = Corpus(load_corpus('test_ouput_2_theme', sources=None, date_min='1000/01', date_max='3000/01'))
        self.entities = EntitiesSet(corpus = self.corpus)
        self.entities_processor = EntitiesProcessor(self.entities)
        self.entities_processor = filter_entities_by_loc(['europe'], self.entities_processor, self.corpus)
        self.entities_processor.add_contextes(self.corpus, nb_sentences=1)
        self.entities_processor.remove_redundant_contexts(self.corpus)
        self.eluset = load_data_elus(overwrite=True, output_file='TestElusSet.json', sources=['CR.txt'])
        self.loaded_elusset = load_data_elus(overwrite=False, input_file='TestElusSet.json')


    def test_loaded_mandat(self):
        self.assertEqual(self.eluset.dict_elus['1509488'].mandats[0].couleur_politique, 'SOC')

    def test_saved_mandat(self):
        self.assertEqual(self.loaded_elusset.dict_elus['1509488'].mandats[0].couleur_politique, 'SOC')

    def test_load_saved_corpus(self):
        self.assertEqual(self.loaded_elusset.dict_elus['1509488'].nom, 'ARMOUGOM')

    def test_save_and_load_corpus(self):
        for i, j in zip(self.eluset.dict_elus, self.loaded_elusset.dict_elus):
            self.assertEqual(self.eluset.dict_elus[i].nom, self.loaded_elusset.dict_elus[j].nom)
            self.assertEqual(len(self.eluset.dict_elus[i].mandats), len(self.loaded_elusset.dict_elus[j].mandats))