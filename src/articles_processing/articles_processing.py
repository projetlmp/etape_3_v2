
from config.config_ner import entity_types


def get_dic_article_loc(entitiesset, corpus):
    list_locations = []
    dic_article_locations = {}
    for key in entitiesset.dict_entities.keys():
        list_loc_entity = []
        entity = entitiesset.dict_entities[key]
        if entity.type_entity == entity_types['lieu']:
            location_names = entity.loc
            for loc in location_names:
                list_loc_entity.append(loc)
                list_locations.append(loc)

            for article_id in entity.articles:
                try:
                    dic_article_locations[article_id].update(list_loc_entity)
                except KeyError:
                    dic_article_locations[article_id] = set(list_loc_entity)
    return dic_article_locations, set(list_locations)

