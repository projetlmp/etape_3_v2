import os
import string

from config.path import path_to_data
from src.custom_class.bow import BoV, Word2vec
from utils.utils import undesirable_words

w2v = Word2vec(os.path.join(path_to_data, 'wiki.fr.vec'), nmax=500000)
bow = BoV(w2v)


def article_processing(article):
    article = article.lower()
    translator = str.maketrans(string.punctuation, ' ' * len(string.punctuation))
    article = article.translate(translator)
    article = article.rstrip().split()
    article = [i for i in article if i not in undesirable_words]
    i = 0
    while i < len(article):
        article[i] = ''.join([j for j in article[i] if not j.isdigit()])
        if len(article[i]) < 3:
            article.pop(i)
        else:
            i += 1
    return article


def process_corpus_for_embedding(corpus_init):
    corpus = corpus_init.copy()
    for article_id in corpus:
        corpus[article_id]['processed_for_embedding_body'] = article_processing(corpus[article_id]['corps'])
    return corpus


def article_embedding(idf_c, article):
    a_e = 0
    for w in article['processed_for_embedding_body']:
        if w in bow.w2v.word2vec:
            a_e = a_e + idf_c[w] * bow.w2v.word2vec[w]
    return a_e


def construct_dictionary_corpus_embedding(corpus):
    dictionary_corpus_embedding = {}
    corpus = process_corpus_for_embedding(corpus)
    idf_c = bow.build_idf(corpus)
    for article_id in corpus:
        dictionary_corpus_embedding[str(corpus[article_id]['id'])] = article_embedding(idf_c, corpus[article_id])
    return dictionary_corpus_embedding


def construct_dictionary_articles_score(dictionary_corpus_embedding):
    score = {}
    for idt_1 in dictionary_corpus_embedding:
        for idt_2 in dictionary_corpus_embedding:
            if idt_1 != idt_2:
                if str(idt_1) + ' ' + str(idt_2) in score.keys():
                    pass
                else:
                    s = bow.score(dictionary_corpus_embedding[idt_1], dictionary_corpus_embedding[idt_2])
                    score[str(idt_1) + ' ' + str(idt_2)] = s
                    score[str(idt_2) + ' ' + str(idt_1)] = s
    return score
