from src.clean_entities.score_bow_articles import construct_dictionary_articles_score, construct_dictionary_corpus_embedding


def remove_similar_articles_from_contexte(dictionary_articles_score, entity, threshold=0.99):
    high_score = {k: dictionary_articles_score[k] for k in dictionary_articles_score if
                  dictionary_articles_score[k] > threshold}
    to_remove = []
    for h_s in high_score:
        if h_s in high_score:
            to_remove.append(h_s.split(' ')[1])
        high_score_to_keep = [h_s for h_s in high_score if h_s.split(' ')[0] not in to_remove]
        high_score = {h_s: high_score[h_s] for h_s in high_score if h_s in high_score_to_keep}
    return [c for c in entity.contextes if c.id_article not in to_remove], to_remove


def clean_similar_articles_from_contextes(corpus, entity):
    dictionary_corpus_embedding = construct_dictionary_corpus_embedding(corpus)
    dictionary_articles_score = construct_dictionary_articles_score(dictionary_corpus_embedding)
    non_redundant_contexte, articles_to_remove= remove_similar_articles_from_contexte(dictionary_articles_score, entity)
    return non_redundant_contexte, articles_to_remove