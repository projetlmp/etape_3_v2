# -*- coding: utf-8 -*-

import json
import os


def load_json(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)


def get_articles_path(articles_basepath):
    """
    :param articles_basepath: path under which all articles of interest are supposed to be stored.
    :return: list of article paths under the directory. An article is defined as a json file under the directory
    """
    list_of_article_paths = []
    for articles_basepath, subdirs, files in os.walk(articles_basepath):
        for name in [f for f in files if (f[0] != '.')]:
            list_of_article_paths.append(os.path.join(articles_basepath, name))
    return list_of_article_paths


def get_articles(list_of_article_paths):
    """
    load a json article
    :param list_of_article_paths:
    :return: a corpus (custom class)
    """
    corpus = {}
    for file_path in list_of_article_paths:
        article = load_json(file_path)
        corpus[article['id']] = article
    return corpus


def load_corpus(articles_basepath):
    """
    load the corpus from articles in the basepath
    :param articles_basepath:
    :return: a corpus (custom class)
    """
    corpus_path = get_articles_path(articles_basepath)
    corpus = get_articles(corpus_path)
    return corpus
