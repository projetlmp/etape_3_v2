from utils.utils_string_manipulation import to_standard, clean_name
from src.gestion_contextes import sort_chronologically, get_context, format_contexte, choose_most_relevant_context
from src.load_data_assos import load_saved_data_assos
from src.load_data_elus import load_saved_data_elus
from config.config_ner import entity_types
from config.config_elus import ordre_mandat
import json
import os
import re
from input.topics import topics
from utils.utils_for_names import is_dead, choose_name
from utils.path_generation import get_output_data_path
import random
from utils.json_encoder import SetEncoder
from input.dict_loc import dict_loc


class Entity:
    def __init__(self, noms, type, articles, locations, contextes=[], asso=False, elu=False, autre=False):
        self.noms = set(noms)
        if isinstance(articles, list):
            self.articles = set(articles)
        else:
            self.articles = {articles}
        self.type_entity = type
        self.contextes = contextes
        self.lieux = set(locations)
        self.asso = asso
        self.elu = elu
        self.autre = autre

    def is_asso(self, asso_set):
        for nom in self.noms:
            std_text = to_standard(nom)
            if self.asso:
                break
            for asso in asso_set.iter_assos():
                if (to_standard(asso.nom) == std_text):
                    self.asso = asso.id
                    break

    def is_elu(self, elus_set):
        for nom in self.noms:
            std_text = to_standard(nom)
            if self.elu:
                break
            for elu in elus_set.iter_elus():
                if (to_standard(str(elu.prenom) + str(elu.nom)) in std_text):
                    self.elu = elu.id
                    break

    def is_person_size_two(self):
        for nom in self.noms:
            nom_nett = clean_name(nom)
            if (entity_types["personne"] in self.type_entity) and (len(nom_nett.split(' ')) >= 2):
                return True
        return False

    def is_alive(self):
        return len([n for n in self.noms if is_dead(n)]) == 0

    def is_autre(self):
        """
        entity.autre is set to True if it is a person
        """
        self.autre = (self.is_person_size_two() and not (self.is_public_place()))

    def is_public_place(self):
        """
        :return: True if the entity has no contextes where it appears as a person and not as a public place.
        Modify the contextes of the entity to keep only those which don't refer to a public place.
        exemple : if contextes are "stade maurice martin " and "lycée maurice martin", returns True;
        if contextes are "stade maurice martin " and "maurice martin loves spagetti", returns False.
        """
        new_contextes = []
        for contexte in self.contextes:
            for nom in self.noms:
                pattern = r"(parc|lycée|hôpital|stade|avenue|rue|allée|centre|square|salle)" + ' ' + nom
                try:
                    s = re.sub(pattern.lower(), '', contexte.text.lower())
                    if s.find(nom.lower()) != -1:
                        new_contextes.append(contexte)
                        break
                except re.error:
                    new_contextes.append(contexte)
                    break
        self.contextes = new_contextes
        return len(new_contextes) == 0

    def is_located_in(self, list_of_loc):
        is_in = [i for i in range(0, len(list_of_loc)) if
                 to_standard(list_of_loc[i]) in [to_standard(x) for x in self.lieux]]
        return len(is_in), ', '.join(list(set([list_of_loc[indice] for indice in is_in])))

    def return_entity_key(self):
        return "%s (%s)" % (to_standard(str(random.sample(self.noms, 1)[0])), self.type_entity)

    def add_contexte(self, corpus_entity, nb_sentences):
        self.contextes = get_context(self, corpus_entity, nb_sentences=nb_sentences,
                                     list_of_article_ids=self.articles)

    def remove_redundant_context(self, corpus_entity):
        """
        remove context that are similar
        :param corpus_entity:
        :return:
        """
        from src.clean_entities.merge_cosine_similar_contextes import clean_similar_articles_from_contextes
        articles = [c.id_article for c in self.contextes]
        small_corpus = {article["id"]: article for article in corpus_entity.iter_articles() if
                        article["id"] in articles}
        self.contextes, articles_to_remove = clean_similar_articles_from_contextes(small_corpus, self)
        self.mentions = len(self.contextes)
        self.articles = [article for article in self.articles if article not in articles_to_remove]


    def find_themes(self, corpus):
        themes_dico = {t: 0 for t in topics}
        for article in self.articles:
            themes_article = corpus.dict_articles[article]['themes']
            for theme in themes_article:
                if theme in themes_dico:
                    themes_dico[theme] += 1
                else:
                    themes_dico[theme] = 1
        return [{'name': theme, 'pertinence': themes_dico[theme]} for
                theme in themes_dico if themes_dico[theme] > 0]

    def find_pertinence(self, corpus):
        return len([i for i in self.articles if len(corpus.dict_articles[i]['themes']) > 0])

    def get_key_value(self, id_entity, key, corpus):
        output_file_values = {'id': id_entity,
                              'is_elu': 1 if self.elu else 0,
                              'is_association': 1 if self.asso else 0,
                              'is_other': 1 if self.autre else 0,
                              'contextes': sort_chronologically(
                                  format_contexte(
                                      choose_most_relevant_context(self.contextes), corpus)),
                              'themes': self.find_themes(corpus),
                              'pertinence': self.find_pertinence(corpus),
                              'name': choose_name(self.noms),
                              'articles': self.articles,
                              'type_entity': self.type_entity,
                              'location': [dict_loc[i] for i in
                                           self.lieux if i in dict_loc],
                              }
        return output_file_values[key]


class EntitiesSet:
    """
    This class stores and saves several entities
    """

    def __init__(self, **kwargs):
        if 'corpus' in kwargs.keys():
            corpus = kwargs.get('corpus')
            self.dict_entities = corpus.load_entities()
        else:
            if 'dict_entities' in kwargs.keys():
                self.dict_entities = kwargs.get('dict_entities', {})
        self.cooccurrence_matrix = None

    def save_cooccurrence_matrix(self, directory_name):
        l = []
        for a in self.cooccurrence_matrix:
            l.append({a: self.cooccurrence_matrix[a]})
        js = json.dumps(l)
        if not os.path.isdir(get_output_data_path(directory_name)):
            os.makedirs(get_output_data_path(directory_name))
        fp = open(get_output_data_path(os.path.join(directory_name, 'cooccurrence_matrix.json')), 'w')
        fp.write(js)
        fp.close()
        return

    def iter_entities(self):
        for entity_key in self.dict_entities:
            yield self.dict_entities[entity_key]

    def save(self, corpus, directory_name, production=False):
        final = []
        champs_standard = ['id', 'is_elu', 'is_association', 'is_other', 'contextes', 'name']
        if production:
            assos_set = load_saved_data_assos()
            elus_set = load_saved_data_elus()
            champs = ['themes', 'pertinence']
        else:
            champs = ['articles', 'type_entity',  'location']
        champs += champs_standard
        for id_entity in self.dict_entities:
            if not production or (
                    self.dict_entities[id_entity].elu or self.dict_entities[id_entity].asso or self.dict_entities[
                id_entity].autre):
                json_file = {}
                for cle in champs:
                    json_file[cle] = self.dict_entities[id_entity].get_key_value(id_entity, cle, corpus)
                if production:
                    json_file['total_mentions'] = len(self.dict_entities[id_entity].contextes)
                    if json_file['is_association']:
                        json_file['main_location'] = assos_set.dict_assos[self.dict_entities[id_entity].asso].name_loc
                        json_file['association_description'] = assos_set.dict_assos[
                            self.dict_entities[id_entity].asso].objet
                        json_file['name_loc_asso'] = assos_set.dict_assos[self.dict_entities[id_entity].asso].name_loc
                        json_file['date_creat_asso'] = assos_set.dict_assos[
                            self.dict_entities[id_entity].asso].date_creat
                    elif json_file['is_elu']:
                        mandats = sorted(elus_set.dict_elus[self.dict_entities[id_entity].elu].mandats,
                                         key=lambda x: ordre_mandat[x.fonction])
                        json_file['mandates'] = []
                        for i, mandat in enumerate(mandats):
                            json_file['mandates'].append(mandat.__to_dict__())
                        json_file['main_location'] = json_file['mandates'][0]['nom_loc']

                    else:
                        json_file['main_location'] = [i for i in self.dict_entities[id_entity].lieux]
                    json_file['total_mentions'] = len(json_file['contextes'])
                final.append(json_file)
        if production:
            final = sorted(final, key=lambda k: k['total_mentions'], reverse=True)
        js = json.dumps(final, cls=SetEncoder)
        if not os.path.isdir(get_output_data_path(directory_name)):
            os.makedirs(get_output_data_path(directory_name))
        fp = open(get_output_data_path(os.path.join(directory_name, 'entities.json')), 'w')
        fp.write(js)
        fp.close()
        return final
