# coding: utf-8

import numpy as np
import stop_words as sw


stop = sw.get_stop_words('fr')


class Word2vec():
    def __init__(self, fname, nmax=100000):
        self.load_wordvec(fname, nmax)
        self.word2id = dict.fromkeys(self.word2vec.keys())
        self.id2word = {v: k for k, v in self.word2id.items()}
        self.embeddings = np.array(self.word2vec.values())

    def load_wordvec(self, fname, nmax):
        self.word2vec = {}
        i = 0
        with open(fname) as f:
            next(f)
            for i, line in enumerate(f):
                word, vec = line.split(' ', 1)
                self.word2vec[word] = np.fromstring(vec, sep=' ')
                if i == (nmax - 1):
                    break
        return self.word2vec

    def score(self, w1, w2):
        W1 = self.word2vec[w1]
        W2 = self.word2vec[w2]
        score = np.dot(W1, W2) / (np.linalg.norm(W1) * np.linalg.norm(W2))
        return score


class BoV():
    def __init__(self, w2v):
        self.w2v = w2v

    def encode(self, sentences, idf=True):
        # takes a list of sentences, outputs a numpy array of sentence embeddings
        sentemb = []
        if idf is True:
            idf_m = self.build_idf(sentences)
            for sent in sentences:
                s_e = 0
                for w in sent:
                    if w in self.w2v.word2vec:
                        s_e = s_e + idf_m[w] * self.w2v.word2vec[w]
                sentemb.append(s_e)
        else:
            for sent in sentences:
                # mean of word vectors
                sentemb.append(np.mean([self.w2v.word2vec[w] for w in sent if w in self.w2v.word2vec], axis=0))
        return np.vstack(sentemb)

    def score(self, s1, s2):
        # cosine similarity
        score = np.dot(s1, s2) / (np.linalg.norm(s1) * np.linalg.norm(s2))
        return score

    def build_idf(self, corpus):
        idf = {}
        for c in corpus:
            for w in set(corpus[c]['processed_for_embedding_body']):
                idf[w] = idf.get(w, 0) + 1
        for w in idf:
            idf[w] = max(1, np.log10(len(corpus) / (idf[w])))
        return idf
