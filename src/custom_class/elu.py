from src.custom_class.mandat import Mandat
from utils.path_generation import get_data_elus_path
import os
import json
from config.config_elus import ordre_mandat

class Elu():
    def __init__(self, nom, prenom, mandat, id):
        self.nom = nom
        self.prenom = prenom
        self.id = id
        if isinstance(mandat, list):
            self.mandats = mandat
        else:
            self.mandats = [mandat]

    def is_located_in(self, codes_departement=[], codes_insee_commune=[], codes_region=[]):
        """
        list(map(lambda x: x[2: len(x)-1], codes_insee_commune) : insee code in the format of the RNE file from real insee code. In the rne insee codes only contain the 3 last numbers and not the departement number.
        list(map(lambda x: x[0:2], codes_insee_commune)) : departement code from insee code
        :param codes_departement: ['XX', 'XX', ...}]
        :param codes_insee_commune: ['XXXXX', 'XXXXX', ....]
        :param codes_region:
        :return:
        """
        return len([mandat for mandat in self.mandats if ((
                mandat.loc in codes_departement + codes_insee_commune + codes_region)
                    or (mandat.loc in list(map(lambda x: x[0:2], codes_insee_commune))))
                    or (mandat.loc[0:2] in list(map(lambda x: x[0:2], codes_insee_commune)))
                    or (mandat.loc[0:2] in codes_departement)]) > 0

class ElusSet():
    def __init__(self, **kwargs):
        if 'elus' in kwargs.keys():
            elus = kwargs.get("elus")
            self.dict_elus = {}
            for i in range(len(elus['id'])):

                mandat = Mandat(elus['loc'][i], elus['nom_loc'][i], elus['fonction'][i], elus['nuance_pol'][i])
                self.dict_elus[elus['id'][i]] = Elu(elus['nom'][i], elus['prenom'][i], mandat, elus['id'][i])
        else:
            if 'dict_elus' in kwargs.keys():
                self.dict_elus = kwargs.get('dict_elus', {})

    def merge_with(self, elusset):
        """
        merge mandats of two Elus from self and another ElusSet if they have the same id
        :param elusset:
        :return:
        """
        for id_elu in elusset.dict_elus:
            if id_elu in self.dict_elus:
                self.dict_elus[id_elu].mandats = elusset.dict_elus[id_elu].mandats + self.dict_elus[id_elu].mandats
            else:
                self.dict_elus[id_elu] = elusset.dict_elus[id_elu]

    def iter_elus(self):
        for id_elus in self.dict_elus:
            yield self.dict_elus[id_elus]

    def iter_id_elus(self):
        for id in self.dict_elus:
            yield id

    def save(self, output_file='ElusSet.json'):
        json_file = {}
        for id_elu in self.iter_id_elus():
            json_file[str(id_elu)] = {}
            mandats = {}
            sorted_mandats = sorted(self.dict_elus[id_elu].mandats, key= lambda x: ordre_mandat[x.fonction])
            for i, mandat in enumerate(sorted_mandats):
                mandats['mandat_%s' % i] = mandat.__to_dict__()
            json_file[str(id_elu)]['mandats'] = mandats
            json_file[str(id_elu)]['nom'] = self.dict_elus[id_elu].nom
            json_file[str(id_elu)]['prenom'] = self.dict_elus[id_elu].prenom
            json_file[str(id_elu)]['id'] = self.dict_elus[id_elu].id
        js = json.dumps(json_file)
        fp = open(os.path.join(get_data_elus_path(), output_file), 'w')
        fp.write(js)
        fp.close()
