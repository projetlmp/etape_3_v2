from src.load_entities import load_entities_from_article
from src.custom_class.entities import Entity
from utils.utils_for_articles import clean_loc_article


class Corpus:
    """
    This class stores articles and return entities from it
    """

    def __init__(self, dict_articles):
        self.dict_articles = dict_articles

    def iter_articles(self):
        for key in self.dict_articles:
            yield self.dict_articles[key]

    def load_entities(self):
        entity_set = {}
        for id in self.dict_articles:
            entities, loc, themes = load_entities_from_article(self.dict_articles[id])
            new_entities = []
            for ent in entities:
                for type in entities[ent]['type']:
                    entity_key = '%s (%s)' % (ent, type)
                    if not entity_key in new_entities:
                        new_entities.append(entity_key)
                    if not (entity_key in entity_set.keys()):
                        entity_set[entity_key] = Entity(entities[ent]['name'], type, id, list(loc.keys()))
                    entity_set[entity_key].noms.update(entities[ent]['name'])
                    entity_set[entity_key].articles.update([id])
                    entity_set[entity_key].lieux.update(
                        [l for l in loc.keys()])

        return entity_set

    def clean_loc_corpus(self):
        """
        clean loc field of each article of the corpus to keep only those given by the client
        :return: the same corpus, with cleaned location
        """
        for id_article in self.dict_articles:
            self.dict_articles[id_article] = clean_loc_article(self.dict_articles[id_article])
        return
