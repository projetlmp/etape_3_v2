import json
import os
from utils.path_generation import get_data_asso_path

class Asso():
    def __init__(self, nom, loc, id, objet, name_loc, date_creat):
        self.nom = nom
        self.loc = loc
        self.id = id
        self.objet = objet
        self.name_loc = name_loc
        self.date_creat = date_creat

    def is_located_in(self, codes_departement=[], codes_insee_commune=[], codes_region=[]):
        if isinstance(self.loc, str):
            return ((self.loc in (codes_departement + codes_insee_commune + codes_region))
                    or (self.loc[0:2] in list(map(lambda x: x[0:2], codes_insee_commune)))
                    or (self.loc[0:2] in codes_departement))
        else:
            return False


class AssoSet():
    def __init__(self, **kwargs):
        if 'assos' in kwargs.keys():
            assos = kwargs.get("assos")
            self.dict_assos = {}
            for i in range(len(assos['id'])):
                self.dict_assos[assos['id'][i]] = Asso(assos['nom'][i], assos['loc'][i], assos['id'][i],
                                                       assos['objet'][i],  assos['name_loc'][i], assos['date_creat'][i])
        else:
            if 'dict_assos' in kwargs.keys():
                self.dict_assos = kwargs.get('dict_assos', {})

    def iter_assos(self):
        for id_asso in self.dict_assos:
            yield self.dict_assos[id_asso]

    def iter_id_assos(self):
        for id_asso in self.dict_assos:
            yield id_asso

    def save(self, output_file='AssosSet.json'):
        json_file = {}
        for id_asso in self.iter_id_assos():
            json_file[str(id_asso)] = {}
            json_file[str(id_asso)]['nom'] = self.dict_assos[id_asso].nom
            json_file[str(id_asso)]['loc'] = self.dict_assos[id_asso].loc
            json_file[str(id_asso)]['id'] = self.dict_assos[id_asso].id
            json_file[str(id_asso)]['objet'] = self.dict_assos[id_asso].objet
            json_file[str(id_asso)]['name_loc'] = self.dict_assos[id_asso].name_loc
            json_file[str(id_asso)]['date_creat'] = self.dict_assos[id_asso].date_creat
        js = json.dumps(json_file)
        fp = open(os.path.join(get_data_asso_path(), output_file), 'w')
        fp.write(js)
        fp.close()
