from src.custom_class.asso import AssoSet
import pandas as pd
import os
from utils.path_generation import get_data_asso_path
from utils.load_saved_data_assos import load_saved_data_assos
from config.config_assos import rna_name

def load_assos(data):
    return {'loc': data['adrs_codeinsee'], 'nom': data['titre'],
            'id': data['id'], 'objet': data['objet'], 'name_loc': data['adrs_libcommune'], 'date_creat': data['date_creat']}

def load_assos_source():
    return pd.read_csv(os.path.join(get_data_asso_path(), rna_name), encoding='latin', sep=";", dtype=str)

def load_data_assos(overwrite=False, input_file='AssosSet.json', output_file='AssosSet.json'):
    if overwrite:
        assos_set = AssoSet(assos=load_assos(load_assos_source()))
        assos_set.save(output_file)
    else:
        assos_set = load_saved_data_assos(input_file)
    return assos_set


def filter_data_assos_by_loc(assos_set, codes_departement=[], codes_insee_commune=[], codes_region=[]):
    filtered_assos_set = {}
    for id_asso in assos_set.iter_id_assos():
        if assos_set.dict_assos[id_asso].is_located_in(codes_departement, codes_insee_commune, codes_region):
            filtered_assos_set[id_asso] = assos_set.dict_assos[id_asso]
    return AssoSet(dict_assos=filtered_assos_set)