from config.config_elus import config_sources, dict_codes_loc, dict_codes_noms, \
    dict_codes_nuances_pol, dict_codes_id, dict_codes_prenoms, get_id, get_fonction, get_nuance_pol, dict_codes_nom_loc
from src.custom_class.elu import ElusSet
from utils.load_saved_data_elus import load_saved_data_elus
from utils.path_generation import get_data_elus_path
import pandas as pd
import os
"""
[data[dict_codes_loc[source][i]] for i in range(len(dict_codes_loc[source]))].apply(lambda x: ''.join()),
"""


def load_elus(data, source):
    try:
        new_data_loc = data[dict_codes_loc[source]].fillna('')
        loc = new_data_loc.apply(lambda x: ''.join(x), axis=1)
    except TypeError:
        loc = data[dict_codes_loc[source]]
    return {'loc': loc,  'nom': data[dict_codes_noms[source]],
            'prenom': data[dict_codes_prenoms[source]], 'nuance_pol': get_nuance_pol(source, data),
            'id': get_id(source, data), 'fonction': get_fonction(source, data), 'nom_loc': data[dict_codes_nom_loc[source]]}


def load_source(source):
    if source.split('.')[-1] == 'txt':
        return pd.read_csv(os.path.join(get_data_elus_path(), source), encoding="latin", sep="\t", skiprows=[0], dtype=str)
    if source.split('.')[-1] == 'csv':
        return pd.read_csv(os.path.join(get_data_elus_path(), source), encoding='latin', sep=";", dtype=str)


def load_data_elus(overwrite=False, input_file='ElusSet.json', output_file='ElusSet.json', sources=config_sources):
    """
    create elus_set for a list of sources, without duplicates in ids
    :param data_elus_path:
    :return:
    """
    if overwrite:
        elus_set = None
        for source in sources:
            elus = load_elus(load_source(source), source)
            if elus_set:
                elus_set.merge_with(ElusSet(elus=elus))
            else:
                elus_set = ElusSet(elus=elus)
        elus_set.save(output_file)
    else:
        elus_set = load_saved_data_elus(input_file)
    return elus_set


def filter_data_elus_by_loc(elus_set, codes_departement=[], codes_insee_commune=[], codes_region=[]):
    filtered_elusset = {}
    for id_elu in elus_set.iter_id_elus():
        if elus_set.dict_elus[id_elu].is_located_in(codes_departement, codes_insee_commune, codes_region):
            filtered_elusset[id_elu] = elus_set.dict_elus[id_elu]
    return ElusSet(dict_elus=filtered_elusset)
