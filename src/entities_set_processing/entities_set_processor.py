from utils.utils_for_corpus import get_corpus
from utils.utils_string_manipulation import to_standard, clean_name
from config.config_ner import entity_types
from src.custom_class.entities import EntitiesSet
from utils.utils_for_names import is_dead

class EntitiesProcessor:
    def __init__(self, entitiesset):
        self.entities_set = entitiesset


    def keep_only_persons(self):
        """
        filter entities set to keep only entities tagged as persons
        :return:
        """
        entities_set = self.entities_set
        filtered_size_two_persons = {}
        for entity in entities_set.iter_entities():
            if entity.type_entity == entity_types["personne"]:
                filtered_size_two_persons[entity.return_entity_key()] = entity
        self.entities_set = EntitiesSet(dict_entities=filtered_size_two_persons)


    def filter_public_places(self):
        entities_set = self.entities_set
        filtered_persons = {}
        for entity in entities_set.iter_entities():
            if entity.is_public_place():
                pass
            else:
                filtered_persons[entity.return_entity_key()] = entity
        self.entities_set = EntitiesSet(dict_entities=filtered_persons)


    def filtered_entities_loc(self, list_loc_of_interest):
        """
        filter entities to
        :param list_loc_of_interest:
        :return:
        """
        filtered_dict_entity = {}
        for entity in self.entities_set.iter_entities():
            is_in_list, validated_list = entity.is_located_in(list_loc_of_interest)
            if is_in_list:
                filtered_dict_entity[entity.return_entity_key()] = entity
        self.entities_set = EntitiesSet(dict_entities=filtered_dict_entity)

    def filter_location_articles(self, corpus, list_loc_of_interest):
        for entity in self.entities_set.iter_entities():
            set_loc_of_interest_norm = set([to_standard(loc) for loc in list_loc_of_interest])
            list_cleaned_articles = []
            for article_id in entity.articles:
                try:
                    set_loc_article_norm = set(
                        [to_standard(loc) for loc in list(corpus.dict_articles[article_id]['loc'].keys())])
                    if len(set_loc_article_norm.intersection(set_loc_of_interest_norm)) > 0:
                        list_cleaned_articles.append(article_id)
                except KeyError:
                    print('Erreur de clé for article %s mais ok' % article_id)
            entity.cleaned_articles = set(list_cleaned_articles)

    def keep_only_persons_size_two(self):
        entities_set = self.entities_set
        filtered_size_two_persons = {}
        for entity in entities_set.iter_entities():
            for nom in entity.noms:
                nom_nett = clean_name(nom)
                if (entity.type_entity == entity_types["personne"]) and (len(nom_nett.split(' ')) >= 2):
                    filtered_size_two_persons[entity.return_entity_key()] = entity
        self.entities_set = EntitiesSet(dict_entities=filtered_size_two_persons)


    def add_contextes(self, corpus, nb_sentences=1):
        entities_set = self.entities_set
        for entity in entities_set.iter_entities():
            corpus_entity = get_corpus(entity, corpus)
            entity.add_contexte(corpus_entity=corpus_entity, nb_sentences=nb_sentences)
        self.entities_set = entities_set

    def initialize_double_index_dict(self, value=0):
        matrix = {}
        entitiesset = self.entities_set
        for ent_1 in entitiesset.iter_entities():
            try:
                matrix[ent_1.return_entity_key()]
            except KeyError:
                matrix[ent_1.return_entity_key()] = {}
            for ent_2 in entitiesset.iter_entities():
                try:
                    matrix[ent_2.return_entity_key()]
                except KeyError:
                    matrix[ent_2.return_entity_key()] = {}
                matrix[ent_1.return_entity_key()][ent_2.return_entity_key()] = value
                matrix[ent_2.return_entity_key()][ent_1.return_entity_key()] = value
        return matrix

    def create_inverse_dictionary(self):
        dict_article_entities = {}
        entitiesset = self.entities_set
        for ent in entitiesset.iter_entities():
            for j in ent.articles:
                try:
                    dict_article_entities[j].update({ent.return_entity_key()})
                except KeyError:
                    dict_article_entities[j] = {ent.return_entity_key()}
        return dict_article_entities

    def construct_cooccurrence_matrix(self, dict_article_entities):
        matrix = self.initialize_double_index_dict(value=0)
        for id in dict_article_entities:
            for i, ent_1 in enumerate(dict_article_entities[id]):
                for ent_2 in list(dict_article_entities[id])[i:]:
                    matrix[ent_1][ent_2] += 1
                    matrix[ent_2][ent_1] += 1
        return matrix

    def add_cooccurrence(self):
        articles_entities_dict = self.create_inverse_dictionary()
        self.entities_set.cooccurrence_matrix = self.construct_cooccurrence_matrix(articles_entities_dict)
        for entity in self.entities_set.iter_entities():
            for j in entity.articles:
                entity.entites_cooccurrentes.update(articles_entities_dict[j])


    def remove_redundant_contexts(self, corpus):
        for entity in self.entities_set.iter_entities():
            entity.remove_redundant_context(corpus_entity=get_corpus(entity, corpus))
        return self.entities_set


    def keep_only_alive_persons(self):
        l = []
        entities_set = self.entities_set
        filtered_size_two_persons = {}
        for entity in entities_set.iter_entities():
            if len([n for n in entity.noms if is_dead(n)]) == 0:
                filtered_size_two_persons[entity.return_entity_key()] = entity
            else :
                l.append(entity.noms)
                print('delete {}'.format(entity.noms))
                print('article {}'.format(entity.articles))
        self.entities_set = EntitiesSet(dict_entities=filtered_size_two_persons)
        return l