
def load_entities_from_article(article):
    try:
        entities = article['entities']
        loc = article['loc']
        themes = article['themes']
        return entities, loc, themes
    except KeyError:
        print('Pas d\'entités dans l\'article {}'.format(article['id']))
        return {}