from utils.utils_string_manipulation import split_into_sentences
from utils.utils_for_names import get_original_location_name
from src.custom_class.contextes import Contextes


def get_context_per_article(entity, article, sentences_nb):
    sentences = split_into_sentences(article['corps'].replace("\n\n", ""))
    date = article["date"]
    titre = article['titre']
    source = article['source_code']
    output = []
    done = set()
    for i in range(0, len(sentences)):
        c = []
        for nom in entity.noms:
            if nom in sentences[i]:
                if i not in done:
                    for j in range(max(i - sentences_nb, 0), i):
                        if j not in done:
                            c.append(sentences[j])
                            done.add(j)
                    done.add(i)
                    c.append(sentences[i])
                    for j in range(i + 1, min(i + sentences_nb + 1, len(sentences) - 1)):
                        if j not in done:
                            done.add(j)
                            c.append(sentences[j])
        if len(c) > 0:
            contexts = " ".join(c)
            themes = article['themes']
            output.append(Contextes(contexts, article["id"], date, titre, source, themes))
    return output


def get_context(entity, corpus_entity, nb_sentences, list_of_article_ids):
    ctx = [get_context_per_article(entity, corpus_entity.dict_articles[a], nb_sentences) for a in
           list_of_article_ids]
    ctx = [item for sublist in ctx for item in sublist]
    return ctx


def sort_chronologically(contextes):
    return sorted(contextes, key=lambda x: str('').join(x['date'].split('-')), reverse=True)


def format_contexte(contexts, corpus):
    return [{'title': context.titre, 'text': context.text, 'source': context.source,
             'date': context.date,
             'id': context.id_article,
             'location': [{'name': get_original_location_name(location)} for location in
                          corpus.dict_articles[context.id_article]['loc']],
             'themes': [{'name': theme, 'pertinence': 1} for theme
                        in context.themes]} for context in
            contexts]


def choose_most_relevant_context(contexts):
    """

    :param contexts: list of Context objects
    :return: returns the first context in the list of contexts which contains a quote with the following pattern ' << quote >> '
    """
    done = []
    output = []
    for context in contexts:
        if (len(done) > 0) and (context.id_article == done[len(done) - 1]):
            if ('>>' in context.text or '<<' in context.text) and (
                    not '>>' in output[len(output) - 1].text or '<<' in output[len(output) - 1].text):
                output[len(output) - 1] = context
        else:
            done.append(context.id_article)
            output.append(context)
    return output
