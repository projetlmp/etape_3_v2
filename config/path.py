import os

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.join(os.path.abspath(__file__))))
path_source_keys = 'config/config_files'
path_input_data = '/home/marjolaine/Documents/Influenceurs/Elan_Etape_2'
path_output_data = '/home/marjolaine/Documents/Influenceurs/Elan_Etape_3'
path_to_data = '/home/marjolaine/Documents/Influenceurs/data'
path_codes_insee = "laposte_hexasmal.csv"
data_elus_path = "RNE"
data_assos_path = "RNA"
