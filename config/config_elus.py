import math

ordre_mandat = {u"Maire": 0, u"Membre du Conseil Municipal":1, u"Président du groupement": 2, u"Membre de l'EPCI":3,  u"Député": 4, u"Sénateur": 5, u"Conseiller régional": 6,
                u"Conseiller départemental": 7}

config_sources = {
    "CD.txt",
    "CM 01 50.txt",
    "CM 51 OM.txt",
    'CR.txt',
    'Deputes.txt',
    'EPCI.txt',
    "groupements.csv",
    "Maires.txt",
    "Senateurs.txt"
}

dict_codes_loc = {
    "CD.txt": "Code du département",
    "CM 01 50.txt": [u"Code du département (Maire)", u"Code Insee de la commune"],
    "CM 51 OM.txt": [u"Code du département (Maire)", u"Code Insee de la commune"],
    "CR.txt": u"Code du département",
    "Deputes.txt": u"Code du département",
    "EPCI.txt": u"Code département EPCI",
    "groupements.csv": u"Département siège",
    "Maires.txt": [u"Code du département (Maire)", u"Code Insee de la commune"],
    "Senateurs.txt": "Code du département",
}

dict_codes_noms = {
    "CD.txt": u"Nom de l'élu",
    "CM 01 50.txt": u"Nom de l'élu",
    "CM 51 OM.txt": u"Nom de l'élu",
    "CR.txt": u"Nom de l'élu",
    "Deputes.txt": u"Nom de l'élu",
    "EPCI.txt": u"Nom de l'élu",
    "groupements.csv": u"Nom Président",
    "Maires.txt": u"Nom de l'élu",
    "Senateurs.txt": u"Nom de l'élu"
}

dict_codes_prenoms = {
    "CD.txt": u"Prénom de l'élu",
    "CM 01 50.txt": u"Prénom de l'élu",
    "CM 51 OM.txt": u"Prénom de l'élu",
    "CR.txt": u"Prénom de l'élu",
    "Deputes.txt": u"Prénom de l'élu",
    "EPCI.txt": u"Prénom de l'élu",
    "groupements.csv": u"Prénom Président",
    "Maires.txt": u"Prénom de l'élu",
    "Senateurs.txt": u"Prénom de l'élu"
}

dict_codes_nuances_pol = {
    "CD.txt": "Nuance politique (C. Gén.)",
    "CM 01 50.txt": "Nuance politique (C. Mun.)",
    "CM 51 OM.txt": "Nuance politique (C. Mun.)",
    "CR.txt": "Nuance mandat",
    "Deputes.txt": "Nuance politique (Député)",
    "EPCI.txt": "Nuance mandat",
    "Maires.txt": u"Nuance politique (C. Mun.)",
    "Senateurs.txt": u"Nuance politique (Sénateur)"
}
dict_codes_id = {
    "CD.txt": "N° Identification d'un élu",
    "CM 01 50.txt": u"N° Identification d'un élu",
    "CM 51 OM.txt": u"N° Identification d'un élu",
    "CR.txt": u"N° Identification d'un élu",
    "Deputes.txt": u"N° Identification d'un élu",
    "EPCI.txt": u"N° Identification d'un élu",
    "Maires.txt": u"N° Identification d'un élu",
    "Senateurs.txt": u"N° Identification d'un élu",
}

dict_code_fonction = {
    "CD.txt": u"Libellé de fonction",
    "CM 01 50.txt": u"Libellé de fonction",
    "CM 51 OM.txt": u"Libellé de fonction",
    "CR.txt": u"Libellé de fonction",
    "Deputes.txt": u"Libellé de fonction",
    "EPCI.txt": u"Libellé de fonction",
}

dict_codes_nom_loc = {
    "Maires.txt": u"Libellé de la commune",
    "CD.txt": u"Libellé du département",
    "CM 01 50.txt": u"Libellé de la commune",
    "CM 51 OM.txt": u"Libellé de la commune",
    "CR.txt": u"Libellé de la région",
    "Deputes.txt": u"Libellé du département",
    "EPCI.txt": u"Libellé de l'EPCI",
    "groupements.csv": u"Nom du groupement",
    "Senateurs.txt": u"Libellé du département"
}


def get_id(source, data):
    if source in dict_codes_id.keys():
        return data[dict_codes_id[source]]
    else:
        if source == "groupements.csv":
            return list(map(lambda x: 'PRES' + str(x[0]) + str(x[1]),
                            list(zip(data[dict_codes_prenoms[source]], data[dict_codes_noms[source]]))))


def get_nuance_pol(source, data):
    if source in dict_codes_nuances_pol.keys():
        return data[dict_codes_nuances_pol[source]].apply(lambda x: x if (isinstance(x, str)) else 'Pas de nuance politique')
    else:
        if source == "groupements.csv":
            return ['Pas de nuance politique'] * len(data[dict_codes_noms[source]])

def get_fonction(source, data):
    if source == "CM 01 50.txt" or source == "CM 51 OM.txt":
        return data[dict_code_fonction[source]].apply(lambda x: x if (not isinstance(x, str)) and (not math.isnan(x)) else 'Membre du Conseil Municipal')
    if source == 'EPCI.txt':
        return data[dict_code_fonction[source]].apply(lambda x: x if (not isinstance(x, str)) and (not math.isnan(x)) else "Membre de l'EPCI")
    if source == "Maires.txt":
        return [u"Maire"] * len(data[dict_codes_noms[source]])
    if source == "groupements.csv":
        return [u"Président du groupement"] * len(data[dict_codes_noms[source]])
    if source == "Senateurs.txt":
        return [u"Sénateur"] * len(data[dict_codes_noms[source]])
    if source == "Deputes.txt":
        return [u'Député']* len(data[dict_codes_noms[source]])
    if source == 'CR.txt':
        return [u"Conseiller régional"]*len(data[dict_codes_noms[source]])
    if source == 'CD.txt':
        return [u'Conseiller départemental']*len(data[dict_codes_noms[source]])

    else:
        return data[dict_code_fonction[source]].apply(lambda x: x if (not isinstance(x, str)) and (not math.isnan(x)) else '')


