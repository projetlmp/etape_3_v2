entity_types = {
    'inconnu': 'UNKNOWN',
    'personne': 'PERS',
    'lieu': 'LOC',
    'organisation': 'ORG',
    'evenement': 'EVENT',
    'work of art': 'WOA',
    'bien de consommation': 'CONSUMER_GOOD',
    'autre': 'OTHER'
}