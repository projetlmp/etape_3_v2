from src.custom_class.entities import EntitiesSet
from src.custom_class.corpus import Corpus
from src.entities_set_processing.entities_set_processor import EntitiesProcessor
from src.load_corpus import load_corpus
from utils.utils_process_entities import process_entities_types
from src.gestion_contextes import sort_chronologically, format_contexte
from utils.utils_for_names import choose_name
import json
from utils.json_encoder import SetEncoder
import os


corpus = Corpus(load_corpus('/home/marjolaine/Documents/Influenceurs/Est_Rep_Etape_2'))
entities = EntitiesSet(corpus=corpus)
output_dir = "/home/marjolaine/Documents/Influenceurs/Est_Rep_Etape_3"
entities_processor = EntitiesProcessor(entities)
dicto = {k: entities_processor.entities_set.dict_entities[k] for k in entities_processor.entities_set.dict_entities if k in list(entities_processor.entities_set.dict_entities.keys())[0:20]}
entities_processor = EntitiesProcessor(EntitiesSet(dict_entities = dicto))
entities_processor.add_contextes(corpus, nb_sentences=1)
entities_processor.remove_redundant_contexts(corpus)



entities_processor.entities_set = process_entities_types(entities_processor.entities_set, overwrite=False,
                                                         codes_departement=['08', '10', '51', '52', '54', '55', '57',
                                                                            '67', '68', '88'],
                                                         codes_insee_commune=['55436', '55005', '55574', '55534',
                                                                              '55247', '55030', '55284', '55575',
                                                                              '55087', '55374', '55133', '55430',
                                                                              '55104', '55326', '55051', '55148',
                                                                              '55315', '55261', '55246', '02813',
                                                                              '55142', '55026', '55352', '55035',
                                                                              '55195', '55075', '55470', '55359',
                                                                              '55059', 'ERROR', '55061', '55335',
                                                                              '55327', '55568', '55001', '55248',
                                                                              '55447', '55031', '55477', '55501',
                                                                              '55144', '55348', '55494', '55296',
                                                                              '55150', '55015', '55516', '55224',
                                                                              '55079', '55215', '55132', '55010'],
                                                         codes_region=['44'])
dict_loc = {'lesroises': 'Les Roises', 'amanty': 'Amanty', 'vouthonbas': 'Vouthon-Bas',
            'vaudevillelehaut': 'Vaudeville-le-Haut', 'horvilleenornois': 'Horville-en-Ornois',
            'baudignecourt': 'Baudignécourt', 'lavincourt': 'Lavincourt', 'vouthonhaut': 'Vouthon-Haut', 'bure': 'Bure',
            'nantlepetit': 'Nant-le-Petit', 'couvertpuis': 'Couvertpuis', 'ribeaucourt': 'Ribeaucourt',
            'chasseybeaupre': 'Chassey-Beaupré', 'maulan': 'Maulan', 'biencourtsurorge': 'Biencourt-sur-Orge',
            'delouzerosieres': 'Delouze-Rosières', 'mandresenbarrois': 'Mandres-en-Barrois',
            'juvignyenperthois': 'Juvigny-en-Perthois', 'hevilliers': 'Hévilliers', 'villerslesec': 'Villers-le-Sec',
            'dainvillebertheleville': 'Dainville-Bertheléville',
            'badonvilliersgerauvilliers': 'Badonvilliers-Gérauvilliers', 'montplonne': 'Montplonne',
            'bazincourtsursaulx': 'Bazincourt-sur-Saulx', 'foucheresauxbois': 'Fouchères-aux-Bois',
            'brauvilliers': 'Brauvilliers', 'saudrupt': 'Saudrupt', 'morley': 'Morley', 'bonnet': 'Bonnet',
            'saintjoire': 'Saint-Joire', 'lebouchonsursaulx': 'Le Bouchon-sur-Saulx',
            'menilsursaulx': 'Ménil-sur-Saulx', 'mauvages': 'Mauvages', 'villesursaulx': 'Ville-sur-Saulx',
            'abainville': 'Abainville', 'houdelaincourt': 'Houdelaincourt', 'ruptauxnonains': 'Rupt-aux-Nonains',
            'baudonvilliers': 'Baudonvilliers', 'savonnieresenperthois': 'Savonnières-en-Perthois',
            'stainville': 'Stainville', 'dammariesursaulx': 'Dammarie-sur-Saulx',
            'montierssursaulx': 'Montiers-sur-Saulx', 'sommelonne': 'Sommelonne', 'lisleenrigault': "L'Isle-en-Rigault",
            'demangeauxeaux': 'Demange-aux-Eaux', 'aulnoisenperthois': 'Aulnois-en-Perthois', 'treveray': 'Tréveray',
            'haironville': 'Haironville', 'brillonenbarrois': 'Brillon-en-Barrois',
            'gondrecourtlechateau': 'Gondrecourt-le-Château', 'cousanceslesforges': 'Cousances-les-Forges',
            'ancerville': 'Ancerville'}

import datetime

for ent in entities_processor.entities_set.iter_entities():
    for c in ent.contextes:
        c.date = datetime.datetime.utcfromtimestamp(c.date / 1000).strftime("%Y-%m-%d")


themes = ['Eoliennes', 'Agriculture', 'Energies vertes', 'Environnement', 'Bure', 'Revendications', 'Energies']

for ent in entities_processor.entities_set.iter_entities():
    ent.themes = {theme: 0 for theme in themes}
    ent.pertinence = 0
    for article in ent.articles:
        themes = corpus.dict_articles[article]['themes']
        for theme in themes:
            if theme in ent.themes:
                ent.themes[theme] += 1
            else:
                ent.themes[theme] = 1
        ent.pertinence = ent.pertinence + 1 if len(themes) > 0 else ent.pertinence
final = []
directory_name = output_dir
production = True
champs_standard = ['id', 'is_elu', 'is_association', 'is_other', 'contextes']
if production:
    assos_set = load_saved_data_assos()
    elus_set = load_saved_data_elus()
    champs = ['themes', 'name', 'pertinence']
else:
    champs = ['noms', 'articles', 'type_entity']
champs += champs_standard
for id_entity in entities.dict_entities:
    if entities.dict_entities[id_entity].elu or entities.dict_entities[id_entity].asso or entities.dict_entities[
        id_entity].autre:
        json_file = {}
        for cle in champs:
            json_file[cle] = entities.dict_entities[id_entity].get_key_value(id_entity, cle, corpus)
        if production:
            json_file['total_mentions'] = len(entities.dict_entities[id_entity].contextes)
            json_file['location'] = [dict_loc[i] for i in
                                     entities.dict_entities[id_entity].lieux if
                                     i in dict_loc]
            if json_file['is_association']:
                json_file['main_location'] = assos_set.dict_assos[entities.dict_entities[id_entity].asso].name_loc
                json_file['association_description'] = assos_set.dict_assos[
                    entities.dict_entities[id_entity].asso].objet
                json_file['name_loc_asso'] = assos_set.dict_assos[entities.dict_entities[id_entity].asso].name_loc
                json_file['date_creat_asso'] = assos_set.dict_assos[
                    entities.dict_entities[id_entity].asso].date_creat
            elif json_file['is_elu']:
                mandats = sorted(elus_set.dict_elus[entities.dict_entities[id_entity].elu].mandats,
                                 key=lambda x: ordre_mandat[x.fonction])
                json_file['mandates'] = []
                for i, mandat in enumerate(mandats):
                    json_file['mandates'].append(mandat.__to_dict__())
                json_file['main_location'] = json_file['mandates'][0]['nom_loc']
            else:
                json_file['main_location'] = [i for i in entities.dict_entities[id_entity].lieux]
            json_file['total_mentions'] = len(json_file['contextes'])
        final.append(json_file)


persons_to_drop = ['nicolashulot (PERS)', 'denischarpentier (PERS)', 'saintmartin (PERS)', 'codecomsaulx (PERS)',
                   'maisondelaresistance (PERS)', 'jeandheurs (PERS)', 'mgrjean (PERS)', 'jeanpaulgusching (PERS)',
                   'jacquesfrechet (PERS)', 'myriamkopp (PERS)', 'saintflorentin (PERS)', 'michelthiery (PERS)',
                   'danielregnier (PERS)', 'michelperrin (PERS)', 'sainteloisaulx (PERS)', 'laurencegeorges (PERS)',
                   'marcsoler (PERS)', 'louisarnould (PERS)', 'stephanebertrand (PERS)', 'yvoncardot (PERS)',
                   'fernandguillemin (PERS)', 'adrienhermann (PERS)', 'corinnefrancois (PERS)', 'emmanuelmacron (PERS)',
                   'etienneambroselli (PERS)', 'mmebiaudet (PERS)', 'abbebaur (PERS)', 'gillesmolitor (PERS)',
                   'francoishumbert (PERS)', 'davidviarrehanen (PERS)', 'remicoutin (PERS)', 'juliebogaert (PERS)',
                   'foyerleclerc (PERS)', 'jeandufour (PERS)', 'franckracaud (PERS)', 'michelregnault (PERS)',
                   'lionelmadella (PERS)', 'jeannavelot (PERS)', 'gerardandre (PERS)',
                   'meusienjean (PERS)', 'jeanfrançoisrenard (PERS)', 'antoinettelarcher (PERS)', 'gérardvolle (PERS)',
                   'robertguyot (PERS)', 'meusienjean (PERS)']

assos_to_keep = ['mirabellorrainenatureenvironnement (ORG)', 'polyvaljapiot (PERS)']

final = sorted(final, key=lambda k: k['total_mentions'], reverse=True)
new_final = [final[i] for i in range(len(final)) if final[i]["id"] not in persons_to_drop]
new_new_final = new_final[0:100] + [i for i in new_final[100:len(new_final) - 1] if (
        ['id'] in assos_to_keep or i['is_elu'] == 1 or ("Eoliennes" in [j['name'] for j in i['themes']]))]

for f in new_new_final:
    for theme in f['themes']:
        if theme['name'] == 'Bure':
            theme['name'] = 'Projet CIGEO'

for f in new_new_final:
    for c in f['contextes']:
        for theme in c['themes']:
            print(theme)

for f in new_new_final:
    if f['total_mentions'] != len(f['contextes']):
        print('pb')

for f in new_new_final:
    if f['total_mentions'] < f['pertinence']:
        print('pb')

assert (len(new_new_final) == 223)
assert (new_new_final[0]['id'] ==  'sebastienlecornu (PERS)')
assert (new_new_final[12]['themes'] == [{'name': 'Projet CIGEO', 'pertinence': 11},
                                        {'name': 'Energies', 'pertinence': 9},
                                        {'name': 'Environnement', 'pertinence': 6},
                                        {'name': 'Revendications', 'pertinence': 1}]
        )
assert (new_new_final[23]['location'] == ['Vouthon-Bas', 'Amanty', 'Gondrecourt-le-Château'])
assert (new_new_final[23]['main_location'] == 'Amanty')
assert (new_new_final[32]['date_creat_asso'] == '1964-07-18')

js = json.dumps(new_new_final, cls=SetEncoder)
if not os.path.isdir(get_output_path(directory_name)):
    os.makedirs(get_output_path(directory_name))
fp = open(get_output_path(os.path.join(directory_name, 'entities_V17_epuron.json')), 'w')
fp.write(js)
fp.close()
